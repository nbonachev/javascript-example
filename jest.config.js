module.exports = {
  clearMocks: true,
  collectCoverage: true,
  coverageDirectory: "coverage",
  coverageReporters: [
    "html",
    "text"
  ],
  testEnvironment: "node",
  moduleFileExtensions: [
    "js",
    "ts",
    "html"
  ],
  transform: {
    "^.+\\.js$": "babel-jest",
    "^.+\\.ts$": "babel-jest",
    "^.+\\.html?$": "html-loader-jest"
  },
  setupFiles: [
    "./test/__mocks__/client.js"
  ]
};
