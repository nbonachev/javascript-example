import { EmailsStorage } from '../../Storage/EmailsStorage';
import { Email } from '../Email/Email';
import { NewEmailInput } from '../NewEmailInput/NewEmailInput';
import { BaseComponent } from '../BaseComponent';

type AddNewEmailFunc = (email: string) => void;

/**
 * Contain Emails and Email Input
 * Main function - load templates
 */
class EmailsList extends BaseComponent {
    private _emailInput: NewEmailInput;
    private _emailsStorage: EmailsStorage;

    constructor(emailsStorage: EmailsStorage, emails: string[]) {
        super(require('./EmailsList.html'));

        this._emailsStorage = emailsStorage;
        this._emailInput = new NewEmailInput(this.addNewEmail.bind(this));

        this.deleteEmailHandler = this.deleteEmailHandler.bind(this);
        this.clickHandler = this.clickHandler.bind(this);

        emails.forEach((email: string) => {
            const newEmail = new Email(email, this.deleteEmailHandler);
            this._emailsStorage.addEmail(newEmail);
        });

        this.initTemplate();
    }

    public getRender(): HTMLElement {
        return this._template;
    }

    public setEmails(rowEmails: string[]): void {
        this._emailsStorage.getEmails().forEach((email) => {
            email.getRender().parentElement!.removeChild(email.getRender());
        });

        const emails = rowEmails.map(
            (email: string) => new Email(email, this.deleteEmailHandler)
        );

        this._emailsStorage.setEmails(emails);
        this._emailsStorage.getEmails().forEach((email: Email) => {
            this._template.insertBefore(
                email.getRender(),
                this._emailInput.getRender()
            );
        });
    }

    public addNewEmail(email: string) {
        const newEmail = new Email(email, this.deleteEmailHandler);
        this._emailsStorage.addEmail(newEmail);
        this._template.insertBefore(
            newEmail.getRender(),
            this._emailInput.getRender()
        );
    }

    protected initTemplate(): void {
        this._emailsStorage.getEmails().forEach((email: Email) => {
            this._template.appendChild(email.getRender());
        });
        this._template.appendChild(this._emailInput.getRender());
        this._template.addEventListener('click', this.clickHandler);
    }

    /**
     * Focus email input if component is clicked
     */
    private clickHandler(event: MouseEvent) {
        if (
            event.target instanceof HTMLElement &&
            event.target.className.indexOf('emails-list') >= 0
        ) {
            this._emailInput.focus();
        }
    }

    private deleteEmailHandler(email: Email): void {
        email.getRender().parentElement!.removeChild(email.getRender());
        this._emailsStorage.removeEmail(email);
    }
}

export { EmailsList, AddNewEmailFunc };
