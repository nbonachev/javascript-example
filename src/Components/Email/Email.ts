import { BaseComponent } from '../BaseComponent';

/**
 * Email component
 * Represents a single email
 */
class Email extends BaseComponent {
    private readonly _email: string;
    private readonly _isValid: boolean;
    private readonly _deleteHandler: (email: Email) => void;

    /**
     * Email Constructor
     * @constructor
     * @param {string} email - the email row value
     * @param {Function} deleteHandler - after delete callback
     */
    constructor(email: string, deleteHandler: (email: Email) => void) {
        super(require('./Email.html'));

        this._email = email;
        this._isValid = Email.validateEmail(email);
        this._deleteHandler = deleteHandler.bind(this);

        this.initTemplate();
    }

    protected initTemplate() {
        const chest = this._template.querySelector('.email-chest')!;
        const emailValue = this._template.querySelector('.email-value')!;

        emailValue.textContent = this._email;

        chest.addEventListener('click', () => this._deleteHandler(this));

        if (this._isValid) {
            this._template.className += ' email_valid';
        } else {
            this._template.className += ' email_invalid';
        }
    }

    get email(): string {
        return this._email;
    }

    public getRender(): HTMLElement {
        return this._template;
    }

    private static validateEmail(email: string) {
        const re = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)])/;
        return re.test(email.toLowerCase());
    }
}

export { Email };
