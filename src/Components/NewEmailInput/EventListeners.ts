import { AddNewEmailFunc } from '../EmailsList/EmailsList';

const focusOutListener = (addNewEmail: AddNewEmailFunc) => (
    event: FocusEvent
) => {
    const value = (event?.target as HTMLInputElement).value;

    if (Boolean(value)) {
        addNewEmail(value.trim());
    }

    (event?.target as HTMLInputElement).value = '';
    event.preventDefault();
};

const keyupListener = (addNewEmail: AddNewEmailFunc) => (
    event: KeyboardEvent
) => {
    const newLineKey = 13;
    const commaKey = 188;

    let email = '';
    const code = event.keyCode;

    const value = (event?.target as HTMLInputElement).value;
    switch (code) {
        case newLineKey:
            email = value.trim();
            break;
        case commaKey:
            email = value.slice(0, -1).trim();
            break;
        default:
            break;
    }
    if (Boolean(email)) {
        addNewEmail(email);
        (event?.target as HTMLInputElement).value = '';
    }
};

const pasteListener = (addNewEmail: AddNewEmailFunc) => (
    event: ClipboardEvent
) => {
    let pastedText;

    if (
        (window as any).clipboardData &&
        (window as any).clipboardData.getData
    ) {
        // IE
        pastedText = (window as any).clipboardData.getData('Text');
    } else {
        pastedText = event?.clipboardData?.getData('text');
    }

    const newLineChar = '\n';
    const commaChar = ',';

    const emails: string[] = [];

    const lines = pastedText?.split(newLineChar);
    lines.forEach((line: string) => {
        const blocks = line.split(commaChar);
        blocks.forEach((block: string) => {
            const email = block.trim();
            if (Boolean(email)) {
                emails.push(email);
            }
        });
    });

    if (emails.length) {
        emails.forEach((email: string) => {
            addNewEmail(email);
        });
    }
    event.preventDefault();
};

export { focusOutListener, keyupListener, pasteListener };
