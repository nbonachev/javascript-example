import { BaseComponent } from '../BaseComponent';
import {
    focusOutListener,
    keyupListener,
    pasteListener,
} from './EventListeners';
import { AddNewEmailFunc } from '../EmailsList/EmailsList';

/**
 * New Emil input component
 * Receives user typo
 */
class NewEmailInput extends BaseComponent {
    private readonly _addNewEmail: AddNewEmailFunc;

    constructor(addNewEmail: AddNewEmailFunc) {
        super(require('./NewEmailInput.html'));
        this._addNewEmail = addNewEmail;
        this.initTemplate();
    }

    public focus() {
        this._template.focus();
    }

    public getRender(): HTMLElement {
        return this._template;
    }

    protected initTemplate(): void {
        this._template.addEventListener(
            'paste',
            pasteListener(this._addNewEmail)
        );
        this._template.addEventListener(
            'keyup',
            keyupListener(this._addNewEmail)
        );
        this._template.addEventListener(
            'focusout',
            focusOutListener(this._addNewEmail)
        );
    }
}

export { NewEmailInput };
