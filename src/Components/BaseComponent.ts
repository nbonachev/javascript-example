/**
 * Base UI-Component
 * Main function - load templates
 */
abstract class BaseComponent {
    protected _template: HTMLElement;

    /**
     * Base UI-Component constructor
     * @constructor
     * @param {string} template - the name of html-template
     */
    protected constructor(template: string) {
        const emailTemplate = document.createElement('div');
        emailTemplate.innerHTML = template.trim();
        this._template = emailTemplate.firstChild! as HTMLElement;
    }

    /**
     * Return HTML representation on UI-Component
     * @function
     */
    abstract getRender(): HTMLElement;

    /**
     * Init template settings
     * @function
     */
    protected abstract initTemplate(): void;
}

export { BaseComponent };
