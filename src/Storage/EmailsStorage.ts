import { Email } from '../Components/Email/Email';
import { EventsManager, EventTypes } from './EventsManager';

/**
 * Store emails
 */
class EmailsStorage {
    private _emails: Email[];

    public eventManager: EventsManager;

    constructor(emails: Email[] = []) {
        this._emails = emails;
        this.eventManager = new EventsManager();
    }

    public getEmails() {
        return this._emails;
    }

    public setEmails(emails: Email[] = []) {
        this._emails = emails;
        this.eventManager.notify(EventTypes.refresh, {
            message: 'all emails has been refreshed',
            value: this.getRowEmails(),
        });
    }

    public getRowEmails(): string[] {
        return this.getEmails().map((email) => email.email);
    }

    public addEmail(email: Email) {
        this._emails.push(email);
        this.eventManager.notify(EventTypes.add, {
            message: 'new email has been added',
            value: email.email,
        });
    }

    public removeEmail(emailForDelete: Email) {
        this._emails = this._emails.filter((email) => email !== emailForDelete);
        this.eventManager.notify(EventTypes.remove, {
            message: 'email has been removed',
            value: emailForDelete.email,
        });
    }

    public getEmailsCount(): number {
        return this._emails.length;
    }
}

export { EmailsStorage };
