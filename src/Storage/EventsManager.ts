enum EventTypes {
    add = 'add',
    remove = 'remove',
    refresh = 'refresh',
}

export type CallbackFunc = (payload: Payload) => void;

type IEnumTpProp = { [key in keyof typeof EventTypes]: CallbackFunc[] };
type Payload = {
    message: string;
    value: string | string[];
};

/**
 * Events Manager
 * Responsible for store subscribers and notify them
 */
class EventsManager {
    private readonly _listeners: IEnumTpProp;

    constructor() {
        this._listeners = {
            add: [],
            remove: [],
            refresh: [],
        };
    }

    /**
     * Add new events listener to particular listeners list
     */
    public subscribe(eventType: EventTypes, callback: CallbackFunc) {
        this._listeners[eventType].push(callback);
    }

    /**
     * Notify listeners for particular events
     */
    public notify(eventType: EventTypes, payload: Payload) {
        this._listeners[eventType].forEach((listener) => listener(payload));
    }
}

export { EventsManager, EventTypes };
