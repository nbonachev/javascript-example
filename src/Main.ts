import { EmailsStorage } from './Storage/EmailsStorage';
import { EmailsList } from './Components/EmailsList/EmailsList';
import { CallbackFunc, EventTypes } from './Storage/EventsManager';

type Options = {
    emails: string[];
};

/**
 * Main entrance Component
 */
class Main {
    private readonly _emailsStorage: EmailsStorage;
    private readonly _emailsList: EmailsList;

    /**
     * Represents a Main Class of EmailInput.
     * @constructor
     * @param {Element} element - The HTML element where EmailInput would be
     * @param {Option} options - Options with possible email array.
     */
    constructor(element: Element | null, options: Options | null) {
        if (!element) {
            throw new Error('Empty html element');
        }

        this._emailsStorage = new EmailsStorage();

        const emails = (options && options.emails) || [];
        this._emailsList = new EmailsList(this._emailsStorage, emails);
        element.appendChild(this._emailsList.getRender());
    }

    /**
     * Return count of emails
     * @function
     */
    public getEmailsCount(): number {
        return this._emailsStorage.getEmailsCount();
    }

    /**
     * Add new email
     * @function
     * @param {string} email - new email address
     */
    public addEmail(email: string): void {
        this._emailsList.addNewEmail(email);
    }

    /**
     * Add new emails. Remove previous ones
     * @function
     * @param {array} emails - new emails
     */
    public setEmails(emails: string[]): void {
        this._emailsList.setEmails(emails);
    }

    /**
     * Subscribe to events
     * @function
     * @param {string} eventName - possible events add|refresh|remove
     * @param {function} callback - new emails
     */
    public subscribeToEvents(eventName: string, callback: CallbackFunc): void {
        this._emailsStorage.eventManager.subscribe(
            eventName as EventTypes,
            callback
        );
    }
}

export { Main };
