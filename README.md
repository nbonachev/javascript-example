# JavaScript Example 

![Master branch coverage](https://gitlab.com/nbonachev/javascript-example/badges/master/coverage.svg)
![Master branch deploy](https://gitlab.com/nbonachev/javascript-example/badges/master/pipeline.svg?job=deploy)

This is `EmailInput` JavaScript example. The main goal of this repository - provide a representation of my capabilities. Of course, I'm not limited to provided JS-technologies. I'm always open to new JS-stack.
Hosted result is [here](https://nbonachev.gitlab.io/javascript-example/).

## API-Documentation

Usage example [index.html](/src/index.html) 

### Connection
Synchronous loading (blocks content) of EmailInput Application
```html
<script src="https://nbonachev.gitlab.io/javascript-example/bundle.js"></script>
```

After connection 
```javascript
// Find place for Email Input
var inputContainerNode = document.querySelector('#emails-input');

// Init EmailsInput. The second argument with predefined emails array is optional
var emailsInput = new EmailsInput(inputContainerNode, {emails: [
      'nbonachev+1@gmail.com',
      'nbonachev@gmail.com'
]});
```

### API
1. Get emails count
```javascript
emailsInput.getEmailsCount();
```
  
2. Add new email
```javascript
emailsInput.addEmail('new@email.com');
```    

3. Refresh email with new array
```javascript
emailsInput.setEmails(['new@email.com', 'new2@email.com']);
```  

4. Subscribe to events.
```javascript
emailsInput.subscribeToEvents('refresh', function (event) {
    console.log(event);
})
```
Possible events:
* refresh - after replacing all entered emails with new one
* remove - after removing email
* add - after adding email


## Developer Guide

These instructions will give you a copy of the project and basic instruction for development and testing.

Requirements:
* Node.js 10+

### Developer mode
Three simple steps
```sh
git clone https://gitlab.com/nbonachev/javascript-example.git
npm i
npm run start
```
After that open http://localhost:8080/.   
Hot reload is set by wepback.

### Running test

For test run use 
```sh
npm run test
```
HTML-Coverage report will generate to `coverage` folder. In a terminal, you may also see the report.

### Linters and prettier
Two commands.
  
TSLint check
```sh
npm run tslint-check
```
Prettier check
```sh
npm run tslint-check
```

### Production build
For a production build run the following command
```sh
npm run build
```
The result will be on `dist` folder. It will contain three files:
* bundle.js - the minified version of an application
* index.html - the usage example
* favicon.png

## CI/CD
In [.gitlab-ci.yml](.gitlab-ci.yml) you may see a simple pipeline of application.
After pushing to remote repo three steps will start:
* Linting
* Test
* Build

On the master branch after successful previous steps will perform deploy to the gitlab pages.

## Built With

Main tools:
* Sass
* TypeScript
* Webpack

## Author
* **Nikita Bonachev** nbonachev@gmail.com
