import { Email } from '../../../src/Components/Email/Email';

describe(Email.name, () => {
    test('Should init email', () => {
        const emailsAddress = 'email';

        const emailEntity = new Email(emailsAddress, () => {});
        expect(emailEntity.email).toBe(emailsAddress);
    });

    test('Should init valid email', () => {
        const emailsAddress = 'email@gmail.com';

        const emailEntity = new Email(emailsAddress, () => {});
        expect(
            emailEntity.getRender().className.indexOf('email_valid')
        ).toBeGreaterThanOrEqual(0);
    });

    test('Should init invalid email', () => {
        const emailsAddress = 'email';

        const emailEntity = new Email(emailsAddress, () => {});
        expect(
            emailEntity.getRender().className.indexOf('email_invalid')
        ).toBeGreaterThanOrEqual(0);
    });

    test('Should return template', () => {
        const emailsAddress = 'email';

        const emailEntity = new Email(emailsAddress, () => {});
        expect(
            emailEntity.getRender().className.indexOf('email')
        ).toBeGreaterThanOrEqual(0);
    });

    test('Should fire chest click', () => {
        const emailsAddress = 'email';
        let isDeleted = false;

        const emailEntity = new Email(emailsAddress, () => (isDeleted = true));
        emailEntity.getRender().querySelector('.email-chest').click();
        expect(isDeleted).toBeTruthy();
    });
});
