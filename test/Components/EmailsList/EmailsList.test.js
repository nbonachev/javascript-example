import { EmailsList } from '../../../src/Components/EmailsList/EmailsList';

describe(EmailsList.name, () => {
    let isDeletedEmail = false;
    let email = null;

    const EmailsStorage = require('../../../src/Storage/EmailsStorage');

    jest.mock('../../../src/Storage/EmailsStorage', () => {
        return jest.fn().mockImplementation(() => {
            return {
                getEmails: function () {
                    return [];
                },
                removeEmail: function () {
                    isDeletedEmail = true;
                },
                addEmail: (newEmail) => (email = newEmail),
            };
        });
    });

    beforeEach(() => {
        isDeletedEmail = false;
        email = null;
    });

    test('Should render empty emails list', () => {
        const emailsList = new EmailsList(new EmailsStorage([]), []);
        expect(
            emailsList.getRender().className.indexOf('emails-list')
        ).toBeGreaterThanOrEqual(0);
    });

    test('should email input place to focus', () => {
        const emailsList = new EmailsList(new EmailsStorage([]), []);
        const previousActiveElement = document.activeElement;
        emailsList.getRender().click();
        expect(previousActiveElement).not.toBe(document.activeElement);
    });

    test('should not email input place to focus', () => {
        const emailsList = new EmailsList(new EmailsStorage([]), []);
        const previousActiveElement = document.activeElement;
        const mockAnotherElement = document.createElement('div');
        mockAnotherElement.className = 'another-mock-elem';
        emailsList.getRender().append(mockAnotherElement);
        emailsList.getRender().querySelector('.another-mock-elem').click();
        expect(previousActiveElement).toBe(document.activeElement);
    });

    test('should delete email', () => {
        new EmailsList(new EmailsStorage([]), ['email@gmail.com']);
        email.getRender().querySelector('.email-chest').click();
        expect(isDeletedEmail).toBeTruthy();
    });
});
