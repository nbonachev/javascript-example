import {
    pasteListener,
    keyupListener,
    focusOutListener,
} from '../../../src/Components/NewEmailInput/EventListeners';

beforeEach(() => {
    delete window.clipboardData;
});

describe(focusOutListener.name, () => {
    test('Should add new email', () => {
        let isAdded = false;
        let isPreventDefaultCall = false;
        const event = {
            target: {
                value: 'new_email',
            },
            preventDefault: () => (isPreventDefaultCall = true),
        };
        focusOutListener(() => (isAdded = true))(event);
        expect(isAdded).toBeTruthy();
        expect(isPreventDefaultCall).toBeTruthy();
        expect(event.target.value).toBe('');
    });

    test('Should not add new email', () => {
        let isAdded = false;
        let isPreventDefaultCall = false;
        const event = {
            target: {
                value: '',
            },
            preventDefault: () => (isPreventDefaultCall = true),
        };
        focusOutListener(() => (isAdded = true))(event);
        expect(isAdded).toBeFalsy();
        expect(isPreventDefaultCall).toBeTruthy();
        expect(event.target.value).toBe('');
    });
});

describe(keyupListener.name, () => {
    test('Should add new email after new line key press', () => {
        let isAdded = false;
        const event = {
            target: {
                value: 'new_email',
            },
            keyCode: 13,
        };

        keyupListener(() => (isAdded = true))(event);
        expect(isAdded).toBeTruthy();
        expect(event.target.value).toBe('');
    });

    test('Should add new email after comma key press', () => {
        let isAdded = false;
        const event = {
            target: {
                value: 'new_email',
            },
            keyCode: 188,
        };

        keyupListener(() => (isAdded = true))(event);
        expect(isAdded).toBeTruthy();
        expect(event.target.value).toBe('');
    });

    test('Should nothing changed', () => {
        let isAdded = false;
        const event = {
            target: {
                value: 'new_email',
            },
            keyCode: 2313,
        };

        keyupListener(() => (isAdded = true))(event);
        expect(isAdded).toBeFalsy();
        expect(event.target.value).toBe('new_email');
    });
});

describe(pasteListener.name, () => {
    test('Should add new emails', () => {
        const addedEmails = [];
        let isPreventDefaultCall = false;

        const event = {
            clipboardData: {
                getData: () => 'new_email, new_email2\nnew_emails3',
            },
            preventDefault: () => (isPreventDefaultCall = true),
        };

        pasteListener((email) => addedEmails.push(email))(event);
        expect(addedEmails).toEqual(['new_email', 'new_email2', 'new_emails3']);
        expect(isPreventDefaultCall).toBeTruthy();
    });

    test('Should add new emails - IE11', () => {
        const addedEmails = [];
        let isPreventDefaultCall = false;

        window.clipboardData = {
            getData: () => 'new_email, new_email2\nnew_emails3',
        };

        const event = {
            preventDefault: () => (isPreventDefaultCall = true),
        };

        pasteListener((email) => addedEmails.push(email))(event);
        expect(addedEmails).toEqual(['new_email', 'new_email2', 'new_emails3']);
        expect(isPreventDefaultCall).toBeTruthy();
    });

    test('Should not add new emails', () => {
        const addedEmails = [];
        let isPreventDefaultCall = false;

        const event = {
            clipboardData: {
                getData: () => '',
            },
            preventDefault: () => (isPreventDefaultCall = true),
        };

        pasteListener((email) => addedEmails.push(email))(event);
        expect(addedEmails).toEqual([]);
        expect(isPreventDefaultCall).toBeTruthy();
    });
});
