import { NewEmailInput } from '../../../src/Components/NewEmailInput/NewEmailInput';

describe(NewEmailInput.name, () => {
    test('Should render new email input', () => {
        const newEmailInput = new NewEmailInput(() => {});
        expect(
            newEmailInput.getRender().className.indexOf('new-email')
        ).toBeGreaterThanOrEqual(0);
    });

    test('Should become in focus', () => {
        const newEmailInput = new NewEmailInput(() => {});
        document.body.append(newEmailInput.getRender());
        newEmailInput.focus();
        expect(document.activeElement).toBe(newEmailInput.getRender());
    });
});
