import { EventsManager } from '../../src/Storage/EventsManager';

describe(EventsManager.name, () => {
    test('Should subscribe to add event', () => {
        const emailsStorage = new EventsManager();
        let eventFired;

        emailsStorage.subscribe('add', (payload) => {
            eventFired = payload;
        });

        emailsStorage.notify('add', 'payload');

        expect(eventFired).toBe('payload');
    });

    test('Should subscribe to remove event', () => {
        const emailsStorage = new EventsManager();
        let eventFired;

        emailsStorage.subscribe('remove', (payload) => {
            eventFired = payload;
        });

        emailsStorage.notify('remove', 'payload');

        expect(eventFired).toBe('payload');
    });

    test('Should subscribe to refresh event', () => {
        const emailsStorage = new EventsManager();
        let eventFired;

        emailsStorage.subscribe('refresh', (payload) => {
            eventFired = payload;
        });

        emailsStorage.notify('refresh', 'payload');

        expect(eventFired).toBe('payload');
    });

    test('Should subscribe to another event', () => {
        const emailsStorage = new EventsManager();
        let eventFired;

        emailsStorage.subscribe('refresh', (payload) => {
            eventFired = payload;
        });

        emailsStorage.notify('add', 'payload');

        expect(eventFired).toBeUndefined();
    });
});
