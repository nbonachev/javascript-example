import { EmailsStorage } from '../../src/Storage/EmailsStorage';

jest.mock('../../src/Components/Email/Email', () => {
    return jest.fn(() => this);
});

const EmailEntity = require('../../src/Components/Email/Email');

describe(EmailsStorage.name, () => {
    test('Should init empty emails storage', () => {
        const emailsStorage = new EmailsStorage();
        expect(emailsStorage.getEmailsCount()).toBe(0);
    });

    test('Should init emails storage with predefined emails', () => {
        const predefinedEmails = [new EmailEntity(), new EmailEntity()];
        const emailsStorage = new EmailsStorage(predefinedEmails);
        expect(emailsStorage.getEmailsCount()).toBe(2);
    });

    test('Should add new email', () => {
        const firstEmail = new EmailEntity();
        const secondEmail = new EmailEntity();

        const predefinedEmails = [firstEmail, secondEmail];
        const emailsStorage = new EmailsStorage(predefinedEmails);
        emailsStorage.addEmail(new EmailEntity());
        expect(emailsStorage.getEmailsCount()).toBe(3);

        const emailsInStorage = emailsStorage.getEmails();

        expect(emailsInStorage.indexOf(firstEmail)).toBeGreaterThanOrEqual(0);
        expect(emailsInStorage.indexOf(secondEmail)).toBeGreaterThanOrEqual(0);
    });

    test('Should remove specific email', () => {
        const emailForDeletion = new EmailEntity();
        const predefinedEmails = [emailForDeletion, new EmailEntity()];
        const emailsStorage = new EmailsStorage(predefinedEmails);
        emailsStorage.removeEmail(emailForDeletion);
        expect(emailsStorage.getEmailsCount()).toBe(1);
        expect(emailsStorage.getEmails().indexOf(emailForDeletion)).toBe(-1);
    });

    test('Should return empty row emails', () => {
        const emailsStorage = new EmailsStorage();
        expect(emailsStorage.getEmailsCount()).toBe(0);
        expect(emailsStorage.getRowEmails()).toEqual([]);
    });

    test('Should return non-empty row emails', () => {
        const emailOne = new EmailEntity();
        emailOne.email = 'one@email.com';
        const emailTwo = new EmailEntity();
        emailTwo.email = 'one@email.com';
        const predefinedEmails = [emailOne, emailTwo];
        const emailsStorage = new EmailsStorage(predefinedEmails);
        expect(emailsStorage.getEmailsCount()).toBe(2);
        expect(emailsStorage.getRowEmails()).toEqual([
            'one@email.com',
            'one@email.com',
        ]);
    });

    test('Should refresh emails storage', () => {
        const emailOne = new EmailEntity();
        emailOne.email = 'one@email.com';
        const emailTwo = new EmailEntity();
        emailTwo.email = 'one@email.com';
        const predefinedEmails = [emailOne, emailTwo];
        const emailsStorage = new EmailsStorage(predefinedEmails);
        expect(emailsStorage.getEmailsCount()).toBe(2);
        expect(emailsStorage.getRowEmails()).toEqual([
            'one@email.com',
            'one@email.com',
        ]);

        const newEmailOne = new EmailEntity();
        newEmailOne.email = 'newemail@email.com';
        emailsStorage.setEmails([newEmailOne]);

        expect(emailsStorage.getEmailsCount()).toBe(1);
        expect(emailsStorage.getRowEmails()).toEqual(['newemail@email.com']);
    });

    test('Should refresh emails with empty storage', () => {
        const emailOne = new EmailEntity();
        emailOne.email = 'one@email.com';
        const emailTwo = new EmailEntity();
        emailTwo.email = 'one@email.com';
        const predefinedEmails = [emailOne, emailTwo];
        const emailsStorage = new EmailsStorage(predefinedEmails);
        expect(emailsStorage.getEmailsCount()).toBe(2);
        expect(emailsStorage.getRowEmails()).toEqual([
            'one@email.com',
            'one@email.com',
        ]);
        emailsStorage.setEmails();
        expect(emailsStorage.getEmailsCount()).toBe(0);
        expect(emailsStorage.getRowEmails()).toEqual([]);
    });
});
