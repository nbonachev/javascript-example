import { Main } from '../src/Main';

describe(Main.name, () => {
    test('Should throw exception', () => {
        expect(() => new Main()).toThrow('Empty html element');
    });

    test('Should init without emails', () => {
        let isAdded = false;

        const element = {
            appendChild: () => (isAdded = true),
        };
        const main = new Main(element);

        expect(isAdded).toBeTruthy();
        expect(main.getEmailsCount()).toBe(0);
    });

    test('Should init with emails', () => {
        let isAdded = false;

        const element = {
            appendChild: () => (isAdded = true),
        };
        const main = new Main(element, {
            emails: ['f@gmai.com', 'second@gmai.com'],
        });

        expect(isAdded).toBeTruthy();
        expect(main.getEmailsCount()).toBe(2);
    });

    test('Should add new email', () => {
        let isAdded = false;

        const element = {
            appendChild: () => (isAdded = true),
        };
        const main = new Main(element, {
            emails: ['f@gmai.com', 'second@gmai.com'],
        });

        expect(isAdded).toBeTruthy();
        expect(main.getEmailsCount()).toBe(2);

        main.addEmail('new@email.com');

        expect(main.getEmailsCount()).toBe(3);
    });

    test('Should receive event', () => {
        let isAdded = false;
        let isEventReceived = false;

        const element = {
            appendChild: () => (isAdded = true),
        };
        const main = new Main(element, {
            emails: ['f@gmai.com', 'second@gmai.com'],
        });

        main.subscribeToEvents('add', () => {
            isEventReceived = true;
        });
        main.addEmail('new@email.com');
        expect(isEventReceived).toBeTruthy();
    });

    test('Should refresh emails', () => {
        let isAdded = false;

        const element = {
            appendChild: () => (isAdded = true),
        };
        const main = new Main(element, {
            emails: ['f@gmai.com', 'second@gmai.com'],
        });

        expect(main.getEmailsCount()).toBe(2);
        main.setEmails(['new@email.com']);
        expect(main.getEmailsCount()).toBe(1);
    });
});
